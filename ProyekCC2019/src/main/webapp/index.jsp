<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>LINE : Free Calls & Messages</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/landing-page.css" rel="stylesheet"/>
        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    </head>
    <body class="landing-page landing-page1">
        <nav class="navbar navbar-transparent navbar-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                    </button>
                    <a href="http://www.creative-tim.com">
                        <div class="logo-container">
                            <div class="logo">
                                <a href="index.jsp"><img src="assets/img/line.png" alt="Creative Tim Logo"></a>
                            </div>
                            <div class="brand">
                                LINE
                            </div>
                        </div>
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="example" >
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#">
                            <i class="fa fa-facebook-square"></i>
                            Like
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            <i class="fa fa-twitter"></i>
                            Tweet
                            </a>
                        </li>
                        <li>
                            <a href="Login.jsp">
                            <i class="fa fa-user"></i>
                            Login/Register
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="wrapper">
            <div class="parallax filter-gradient orange" data-color="orange">
                <div class="parallax-background">
                    <img class="parallax-background-image" src="assets/img/showcases/showcase-1/bg.jpg">
                </div>
                <div class= "container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="description">
                                <h2>Free Messaging Whenever, Wherever</h2>
                                <br>
                                <h5>Send free one-on-one and group texts to your friends anytime, anywhere! LINE is available for a variety of smartphone devices
                                    (iPhone, Android, Windows Phone, BlackBerry, and Nokia)
                                 and even your PC.</h5>
                            </div>
                            <div class="buttons">
                                <button class="btn btn-fill btn-neutral">
                                <i class="fa fa-apple"></i> Appstore
                                </button>
                                <button class="btn btn-simple btn-neutral">
                                <i class="fa fa-android"></i>
                                </button>
                                <button class="btn btn-simple btn-neutral">
                                <i class="fa fa-windows"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-5  hidden-xs">
                            <div class="parallax-image">
                                <img class="phone" src="assets/img/showcases/showcase-1/line_app.png"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-gray section-clients">
                <div class="container text-center">
                    <h4 class="header-text">They were the first to go PlayIT</h4>
                    <p>
                        Build customer confidence by listing your users! Anyone who has used your service and has been pleased with it should have a place here! From Fortune 500 to start-ups, all your app enthusiasts will be glad to be featured in this section. Moreover, users will feel confident seing someone vouching for your product!<br>
                    </p>
                    <div class="logos">
                        <ul class="list-unstyled">
                            <li ><img src="assets/img/logos/adobe.png"/></li>
                            <li ><img src="assets/img/logos/zendesk.png"/></li>
                            <li ><img src="assets/img/logos/ebay.png"/></i>
                            <li ><img src="assets/img/logos/evernote.png"/></li>
                            <li ><img src="assets/img/logos/airbnb.png"/></li>
                            <li ><img src="assets/img/logos/zappos.png"/></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="section section-presentation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="description">
                                <h4 class="header-text">Free Voice & Video Calls</h4>
                                <p>Call friends and family as often as you want,
									for as long as you want.
									Free international voice and video calls
									make it easy to stay connected.
									Currently available for iPhone, iPad, Android,
									Windows Phone, PC (Windows and Mac),
									and LINE Lite on Android.</p>
                                
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1 hidden-xs">
                            <img src="assets/img/showcases/showcase-1/en_2016_02.png"/ style="top:-50px">
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-demo">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="description-carousel" class="carousel fade" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item">
                                        <img src="assets/img/showcases/showcase-1/examples/home_3.jpg" alt="">
                                    </div>
                                    <div class="item active">
                                        <img src="assets/img/showcases/showcase-1/examples/home_2.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="assets/img/showcases/showcase-1/examples/home_1.jpg" alt="">
                                    </div>
                                </div>
                                <ol class="carousel-indicators carousel-indicators-orange">
                                    <li data-target="#description-carousel" data-slide-to="0" class=""></li>
                                    <li data-target="#description-carousel" data-slide-to="1" class="active"></li>
                                    <li data-target="#description-carousel" data-slide-to="2" class=""></li>
                                </ol>
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <h4 class="header-text">Easy to use</h4>
                            <p>
                                With all the apps that users love! Make it easy for users to share, like, post and tweet their favourite things from the app. Be sure to let users know they continue to remain connected while using your app!
                            </p>
                            <a href="http://www.creative-tim.com/product/awesome-landing-page" id="Demo1" class="btn btn-warning btn-fill" data-button="warning">Get Free Demo</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-no-padding">
                <div class="parallax filter-gradient orange" data-color="orange">
                    <div class="parallax-background">
                        <img class ="parallax-background-image flipped" src="assets/img/showcases/showcase-1/bg2.jpg">
                    </div>
                    <div class="info">
                        <h1>Download Line Apps Now!</h1>
                        <p>Free Call & Message</p>
                        <a href="#" class="btn btn-neutral btn-lg btn-fill">DOWNLOAD</a>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Line Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Line Today
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="social-area pull-right">
                        <a class="btn btn-social btn-facebook btn-simple">
                        <i class="fa fa-facebook-square"></i>
                        </a>
                        <a class="btn btn-social btn-twitter btn-simple">
                        <i class="fa fa-twitter"></i>
                        </a>
                        <a class="btn btn-social btn-pinterest btn-simple">
                        <i class="fa fa-pinterest"></i>
                        </a>
                    </div>
                    <div class="copyright">
                        &copy; 2019 <a href="#">Design Team</a>, made by hand & love
                    </div>
                </div>
            </footer>
        </div>

    </div>
    </body>
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.js" type="text/javascript"></script>
    <script src="assets/js/awesome-landing-page.js" type="text/javascript"></script>
</html>
