<!DOCTYPE html>
<html lang="en">
<head>
	<title>LINE : Panggilan & Pesan Gratis</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-26">
						LINE
					</span>
					<span class="login100-form-title p-b-48">
					  <a href="index.jsp"><img src="assets/img/line.png" alt="Creative Tim Logo" style="width:70px;height:70px;"></a>
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
						<input class="input100" type="text" name="tEmail">
						<span class="focus-input100" data-placeholder="Email"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="tPass">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" type="submit" name="blogin">
								Login
							</button>
						</div>
					</div>

					<div class="text-center p-t-115">
						<span class="txt1">
							Do not have an account?
						</span>

						<a class="txt2" href="newreg.jsp">
							Sign Up
						</a>
					</div>
				</form>
				<jsp:useBean id="allmethod" class="edu.stts.allmethod"></jsp:useBean>
			</div>
		</div>
	</div>
	<!--Import date buat cookie-->
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%
	String email=request.getParameter("tEmail");
	String pass=request.getParameter("tPass");
	
	if (request.getParameter("blogin")!=null)
	{
		if (pass!="" && email !="")
		{
			//cookie
			Date now = new Date();
			String timestamp = now.toString();
			Cookie cookie = new Cookie ("email",email);
			cookie.setMaxAge(365 * 24 * 60 * 60);
			response.addCookie(cookie);
			
			//out.println("<script>alert('"+allmethod.connectdb()+"')</script>");	
			//out.println("<script>alert('"+allmethod.newuser(email,hp,pass)+"')</script>");
			if(allmethod.cekUser(email, pass) == "1"){
				response.sendRedirect("home.jsp");
			}else{
				out.println("<script>alert('Gagal login!')</script>");
			}
		}else{
			out.println("<script>alert('Lengkapi data dahulu!')</script>");
		}
	}
%>

<%
if (request.getParameter("blogin")!=null)
{
	//response.sendRedirect("home.jsp");
}
if (request.getParameter("bregis")!=null)
{
	response.sendRedirect("Register.jsp");
}
%>

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>