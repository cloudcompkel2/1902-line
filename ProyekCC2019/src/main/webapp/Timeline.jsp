<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  	<style type="text/css">
		.bodyMore {width: 100%; height:40%; text-align:center; float:left; padding-top:5%;}
		.bodyContact {width: 100%; height:10%; text-align : left;}
	</style>
    <!-- Required meta tags -->
    <meta charset="ISO-8859-1">
	<title>LINE : Panggilan & Pesan Gratis</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!--font icon css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>LINE : Panggilan & Pesan Gratis!</title>
  </head>
  <body>
  <% 
  	Cookie cookies [] = request.getCookies();
  	Cookie email = null;
  	if(cookies != null){
  		for(int i=0; i<cookies.length; i++){
  			if(cookies[i].getName().equals("email")){
  				email = cookies[i];
  				break;
  			}
  		}
  	}
  %>
  <form>
	<!-- nav 1 -->
	<nav class="navbar navbar-expand-lg navbar-fixed-top navbar-light bg-light">
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		  <li class="nav-item">
			<h2><%=email.getValue()%>Friends 0</h2>
		  </li>
		</ul>
		<a data-toggle="modal" data-target="#exampleModal"><i class="fa fa-search fa-2x" aria-hidden="true" style="margin-right:30px"></i></a>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Nama</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<form>
				  <div class="form-group">
					<input type="text" class="form-control" id="recipient-name">
				  </div>
				</form>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-primary">Search</button>
			  </div>
			</div>
		  </div>
		</div>
		<a href="profile.jsp"><i class="fa fa-cog fa-2x" aria-hidden="true"></i></a>
	  </div>
	</nav>
	<!-- nav 2 -->
	<nav class="nav-fill navbar-fixed-top bg-light">
	  <div class="nav nav-tabs" id="nav-tab" role="tablist">
		<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" onclick="window.location.href='home.jsp'" role="tab" aria-controls="nav-contact" aria-selected="true"><i class="fa fa-user fa-2x" aria-hidden="true"></i></a>
		<a class="nav-item nav-link" id="nav-chat-tab" data-toggle="tab" onclick="window.location.href='Chat.jsp'" role="tab" aria-controls="nav-chat" aria-selected="false"><i class="fa fa-commenting-o fa-2x" aria-hidden="true"></i></a>
		<a class="nav-item nav-link active" id="nav-timeline-tab" data-toggle="tab" onclick="window.location.href='Timeline.jsp'" role="tab" aria-controls="nav-timeline" aria-selected="false"><i class="fa fa-clock-o fa-2x" aria-hidden="true"></i></a>
		<a class="nav-item nav-link" id="nav-more-tab" data-toggle="tab" href="#nav-more" role="tab" aria-controls="nav-more" aria-selected="false"><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i></a>
	  </div>
	</nav>
	<div class="tab-content" id="nav-tabContent">
	  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
	   <button class="bodyContact" style="background-color:transparent; border:none;">
		<i class="fa fa-user fa-2x" aria-hidden="true"></i>User
	   </button>
	  </div>
	  <div class="tab-pane fade show active" id="nav-chat" role="tabpanel" aria-labelledby="nav-chat-tab">...</div>
	  <div class="tab-pane fade" id="nav-timeline" role="tabpanel" aria-labelledby="nav-timeline-tab">...</div>
	  <div class="tab-pane fade" id="nav-more" role="tabpanel" aria-labelledby="nav-more-tab">
		<div class="bodyMore">
		 <button type="button" class="btn-primary-outline" style="background-color:transparent; border:none; width : 33%;" data-toggle="modal" data-target="#modalAddFriend">
		  <i class="fa fa-user-plus fa-5x" aria-hidden="true"></i>
		 </button>
			<div class="modal fade" id="modalAddFriend" tabindex="-1" role="dialog" aria-labelledby="modalAddFriend" aria-hidden="true">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>
				  <div class="modal-body">
					<form>
					  <div class="form-group">
						<input type="text" class="form-control" id="recipient-name" placeholder="search by id user">
					  </div>
					</form>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-primary">Search</button>
				  </div>
				</div>
			  </div>
			</div>
		 <button type="button" class="btn-primary-outline" style="background-color:transparent; border:none; width : 33%;"><i class="fa fa-users fa-5x" aria-hidden="true"></i></button>
		 <button type="submit" class="btn-primary-outline" style="background-color:transparent; border:none; width : 33%;"><i class="fa fa-user fa-5x"></i></button>
	  </div>
	</div>
	</form>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <jsp:useBean id="allmethod" class="edu.stts.allmethod"></jsp:useBean>
  </body>
</html>