<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  	<style type="text/css">
		.bodyMore {width: 100%; height:40%; text-align:center; float:left; padding-top:5%;}
		.bodyContact {width: 100%; height:10%; text-align : left;}
	</style>
	<style>
			/* CSS untuk Pesan*/                                
		<style>
body {
  margin: 0 auto;
  max-width: 800px;
  padding: 0 20px;
}

.container {
  border: 2px solid #dedede;
  background-color: #f1f1f1;
  border-radius: 5px;
  padding: 10px;
  margin: 10px 0;
}

.darker {
  border-color: #ccc;
  background-color: #ddd;
}

.container::after {
  content: "";
  clear: both;
  display: table;
}

.container img {
  float: left;
  max-width: 60px;
  width: 100%;
  margin-right: 20px;
  border-radius: 50%;
}

.container img.right {
  float: right;
  margin-left: 20px;
  margin-right:0;
}

.time-right {
  float: right;
  color: #aaa;
}

.time-left {
  float: left;
  color: #999;
}

/*CSS PESAN*/
	</style>
<style type="text/css">
table td{
border:none;
}
table{
border:none;
}
</style>
<style>
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}

.button2 {background-color: #008CBA;} /* Blue */
.button3 {background-color: #f44336;} /* Red */ 
.button4 {background-color: #e7e7e7; color: black;} /* Gray */ 
.button5 {background-color: #555555;} /* Black */
</style>
    <!-- Required meta tags -->
    <meta charset="ISO-8859-1">
	<title>LINE : Panggilan & Pesan Gratis</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!--font icon css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  </head>
<body>
<jsp:useBean id="allmethod" class="edu.stts.allmethod"></jsp:useBean>
<% 
  	Cookie cookies [] = request.getCookies();
  	Cookie email = null;
  	if(cookies != null){
  		for(int i=0; i<cookies.length; i++){
  			if(cookies[i].getName().equals("email")){
  				email = cookies[i];
  				break;
  			}
  		}
  	}
  %>
  <form method="get" action="home.jsp">
	<!-- nav 1 -->
	<nav class="navbar navbar-expand-lg navbar-fixed-top navbar-light bg-light">
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		  <li class="nav-item">
			<h3 style="text-transform :uppercase;"><%= allmethod.namauser(email.getValue())%> &nbsp; &nbsp;</h3>
			<h3> Friends <% out.print(allmethod.countfriend((email.getValue()),allmethod.namauser(email.getValue()))); %></h3>
		  </li>
		</ul>
		<a data-toggle="modal" data-target="#exampleModal"><i class="fa fa-search fa-2x" aria-hidden="true" style="margin-right:30px"></i></a>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
		
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Nama</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			 <form method="get" action="home.jsp">
			  <div class="modal-body">
			
				  <div class="form-group">
		
					<input type="text" class="form-control" id="recipient-name" name="search-friend">
				
				 	
				  </div>
		<div class="modal-footer">
				  
				<button type="submit" name="btnSearch" class="btn btn-primary">Search</button>
				
			  </div>
				  
			  </div>
			  </form>
			  <%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
	<!--		  <%
	String id=request.getParameter("recipient-name");
	
	if (request.getParameter("id")!=null)
	{
		if (id!="")
		{
			if(allmethod.cekId(id) == "1"){
				
			}else{
				out.println("<script>alert('Gagal login!')</script>");
			}
		}else{
			out.println("<script>alert('Lengkapi data dahulu!')</script>");
		}
	}
%>-->
			</div>
		  </div>
		</div>
		   <!-- <a href="profile.jsp"><i class="fa fa-cog fa-2x" aria-hidden="true">  </i></a> -->
		<div class="w3-dropdown-hover">
		    <button class="w3-button w3-black">Settings</button>
		    <div class="w3-dropdown-content w3-bar-block w3-border">
		      <a href="profile.jsp" class="w3-bar-item w3-button">My Profile</a>
		      <a href="Login.jsp" class="w3-bar-item w3-button">Sign Out</a>
		    </div>
	  </div>
		  
	  </div>
	</nav>
	<!-- nav 2 -->
	<nav class="nav-fill navbar-fixed-top bg-light">
	  <div class="nav nav-tabs" id="nav-tab" role="tablist">
		<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" onclick="window.location.href='home.jsp'" role="tab" aria-controls="nav-contact" aria-selected="true"><i class="fa fa-user fa-2x" aria-hidden="true"></i></a>
		<a class="nav-item nav-link active" id="nav-more-tab" data-toggle="tab" href="#nav-more" role="tab" aria-controls="nav-more" aria-selected="false"><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i></a>
	  </div>
	</nav>
	<div class="tab-content" id="nav-tabContent">
	  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
	   <button class="bodyContact" style="background-color:transparent; border:none;">
		<i class="fa fa-user fa-2x" aria-hidden="true"></i>User
	   </button>
	  </div>
	 <div class="tab-pane fade show active" id="nav-more" role="tabpanel" aria-labelledby="nav-more-tab">
	 <form method="get" action="multichat.jsp">
	 <table border="1">
	 <tr>
	<td>
	  <%
				 String datamulti=allmethod.namauser(email.getValue())+"-"+email.getValue();
				 if (request.getParameter("datamulti")!=null)
				  {
					 datamulti=request.getParameter("datamulti");
				  }
				/*if (request.getParameter("btnAdd")!=null)
				 	{
					 datamulti+=request.getParameter("datauser")+";";
				 	}*/
				 if (request.getParameter("btnMulti")!=null)
				  {
					 	
						String select = allmethod.selectAllfriend(email.getValue(),"");
						String [] data = select.split(";");
						String [][] datajadi= new String[data.length][2];
						
						if (data.length!=0)
						{
							for (int i=1;i<data.length;i++)
							{				
						       String[] temp= data[i].split("-");
							   datajadi[i][0]= temp[0];
							   datajadi[i][1]= temp[1];
							   
							}
							for (int j=1;j<datajadi.length;j++)
							{
								out.println("<table>");
								out.println("<tr>");
								out.println("<td><form method='get' action='multichat.jsp'><div class='bodyContact' style='background-color:transparent; border:1px solid white;'>"
											+"<i class='fa fa-user-circle-o fa-3x' aria-hidden='true'></i><input type='hidden' name='btnMulti' value='a'><input type='hidden' name='datamulti' value='"+datamulti+";"+datajadi[j][0]+"-"+datajadi[j][1]+"'><input type='hidden' name='datauser' value='"+datajadi[j][0]+"-"+datajadi[j][1]+"'>  "+datajadi[j][0]
											+" &nbsp; &nbsp;&nbsp;&nbsp;</td> <td><button class='button button4' type='submit' name='btnAdd'>Add To Multichat</button> </div></form> </td>");
								out.println("</tr>");
								out.println("</table>");
							}
				  
						}
					}
				 
				 	
	   			 
				%>
	 
		</td>
		
		<td>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type='hidden' name='btnMulti' value='a'>
		<button class="button button5" type="submit" name="btnMake">Make Multichat Room</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		</tr>
		</table>
		</form>
	  </div>
		</div>
	</form>
	<%
	 if (request.getParameter("btnMake")!=null)
	  {
		 if (request.getParameter("datamulti")!=null)
		  {
			 datamulti=request.getParameter("datamulti");
		  }
		 //out.print(datamulti);
	  		allmethod.startmultichat(datamulti);
	  }
	%>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
 

</body>
</html>