<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<style>
	.coverProfile {width:100%; height:25%; position: relative; text-align: center;}
	.coverProfile .imgCover{width:100%; height:250px;}
	.coverProfile .btnCover{
		position: absolute;
		top: 88%;
		left: 97%;
		background-color: Transparent;
		color: black;
		font-size: 16px;
		border: none;
		cursor: pointer;
		border-radius: 5px;
	}
	.coverProfile .bntHome{
		position: absolute;
		top: 79%;
		left: 1%;
		background-color: Transparent;
		color: black;
		font-size: 16px;
		padding: 6px 12px;
		border-width: 2px;
		border-color:black;
		border-radius: 5px;
		cursor: pointer;
	}
	.coverProfile .imgProfile{
		border-radius: 50%;
		position : absolute;
		top:29%;
		left:47%;
		width:100px;
		height:100px;
	}
	.coverProfile .btnProfile{
		position: absolute;
		top: 56%;
		left: 51%;
		background-color: Transparent;
		color: black;
		font-size: 16px;
		border: none;
		cursor: pointer;
		border-radius: 5px;
	}
	.data{
		width:100%;
		<
		float:left;
		>
	}
	.data .text{
		width:100%;
	}
	.data .btn{
		width:100%;
		background-color:
	}
</style>
	<meta charset="ISO-8859-1">
	<title>Edit Profile</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!--font icon css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<jsp:useBean id="allmethod" class="edu.stts.allmethod"></jsp:useBean>
<% 
	Cookie cookies [] = request.getCookies();
	Cookie email = null;
	if(cookies != null){
		for(int i=0; i<cookies.length; i++){
			if(cookies[i].getName().equals("email")){
				email = cookies[i];
				break;
			}
		}
	}
	String select = allmethod.selectUser(email.getValue());
	String [] data = select.split(";");
	//email->pass->hp->nama->status->birthday->update
	//0    ->1   ->2 ->3   ->4     ->5       ->6
%>
<form>
	<div class="coverProfile">
		<img class="imgCover" src="" onerror="this.onerror=null;this.src='defaultBackground.png';">
		<button class="btnCover"><i class="fa fa-camera"></i></button>
		<input name="bhome" type="submit" class="bntHome" value="Home"></input>
		<img class="imgProfile" src="" onerror="this.onerror=null;this.src='defaultUserIcon.png';">
		<button class="btnProfile"><i class="fa fa-camera"></i></button>
	</div>
	<div class="data">
		Display name</br>
		<input name="txtname" class="text" type="text" value="<% out.print(data[3]);%>"></input>
		Status message</br>
		<textarea name="txtstatus" class="text" rows="5" style="overflow:scroll; resize:none; overflow-x: hidden;" placeholder="<% if(data[6].equals("0")) out.print(data[4]);%>"><% if(data[6].equals("1")) out.print(data[4]);%></textarea>
		Phone number</br>
		<input name="txthp" class="text" type="text" value="<% out.print(data[2]);%>"></input>
		ID</br>
		<input class="text" type="text" value="" disabled></input>
		Birthday</br>
		<input name="txtbd" class="text" type="date" value="<% if(!data[5].equals("NULL")) out.print(data[5]); %>"></input>
		<input name="bsave" type="submit" class="btn btn-primary" value="Save"></input>
	</div>
	
  </form>
  
  
  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
	<!-- Load polyfills to support older browsers -->
	<script src="//polyfill.io/v3/polyfill.min.js?features=es2015%2CIntersectionObserver" crossorigin="anonymous"></script>
</body>
<%

String status = request.getParameter("txtstatus");
String name = request.getParameter("txtname");
String hp = request.getParameter("txthp");
String bd = request.getParameter("txtbd");
String update = "1";
if(status == ""){
	status = "No Status";
	update = "0";
}
if (request.getParameter("bhome")!=null)
{
	response.sendRedirect("home.jsp");
}
if (request.getParameter("bsave")!=null)
{
	if(name!=null && hp!=null){
		out.println("<script>alert('"+allmethod.updateUser(email.getValue(), name, hp, status, bd, data[1], update)+"')</script>");
		response.sendRedirect("profile.jsp");
	}
}
%>
</html>