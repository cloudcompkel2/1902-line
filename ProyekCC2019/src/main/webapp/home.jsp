<%@page import="edu.stts.allmethod"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
  <head>
  	<style type="text/css">
		.bodyMore {width: 100%; height:40%; text-align:center; float:left; padding-top:5%;}
		.bodyContact {width: 100%; height:10%; text-align : left;}
	</style>
	<style>
			/* CSS untuk Pesan*/                                
		<style>
body {
  margin: 0 auto;
  max-width: 800px;
  padding: 0 20px;
}

.container {
  border: 2px solid #dedede;
  background-color: #f1f1f1;
  border-radius: 5px;
  padding: 10px;
  margin: 10px 0;
}

.darker {
  border-color: #ccc;
  background-color: #ddd;
}

.container::after {
  content: "";
  clear: both;
  display: table;
}

.container img {
  float: left;
  max-width: 60px;
  width: 100%;
  margin-right: 20px;
  border-radius: 50%;
}

.container img.right {
  float: right;
  margin-left: 20px;
  margin-right:0;
}

.time-right {
  float: right;
  color: #aaa;
}

.time-left {
  float: left;
  color: #999;
}
/*CSS PESAN*/
	</style>
    <!-- Required meta tags -->
    <meta charset="ISO-8859-1">
	<title>LINE : Panggilan & Pesan Gratis</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="w3.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css">
	<!--font icon css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  </head>
  <body>
  <% 
  	Cookie cookies [] = request.getCookies();
  	Cookie email = null;
  	if(cookies != null){
  		for(int i=0; i<cookies.length; i++){
  			if(cookies[i].getName().equals("email")){
  				email = cookies[i];
  				break;
  			}
  		}
  	}
  %>
  <jsp:useBean id="allmethod" class="edu.stts.allmethod"></jsp:useBean>
  
	<!-- nav 1 -->
	<nav class="navbar navbar-expand-lg navbar-fixed-top navbar-light bg-light" >
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01"> 
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		  <li class="nav-item">
			<h3 style="text-transform :capitalize;"><%= allmethod.namauser(email.getValue())%> &nbsp; &nbsp;</h3>
			<h3> Friends <% out.print(allmethod.countfriend((email.getValue()),allmethod.namauser(email.getValue()))); %></h3>
		  </li>
		</ul>
		<a data-toggle="modal" data-target="#exampleModal"><i class="fa fa-search fa-2x" aria-hidden="true" style="margin-right:30px"></i></a>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
		
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Nama</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  	<form method="get" action="home.jsp">
			  <div class="modal-body">
			
				  <div class="form-group">
				  
				  	
				  	
					<input type="text" class="form-control" id="recipient-name" name="search-friend">
				 <!-- <%
				  /*if (request.getParameter("btnSearch")!=null)
				  {
					    String select = allmethod.selectUser(request.getParameter("search-name"));
						String [] data = select.split(";");
						if (data.length!=0)
						{
							for (int i=1;i<data.length;i++)
							{
								out.println("<button class='bodyContact' style='background-color:transparent; border:none;'>"+
											"<i class='fa fa-user-circle-o fa-3x' aria-hidden='true'></i>  "+data[i]+" </button> ");
							}
						}
						else
						{
							out.println("data=0");
						}
				  }	   				 
	   			 */
				%>-->
				 	
				  </div>
		<div class="modal-footer">
				  
				<button type="submit" name="btnSearch" class="btn btn-primary">Search</button>
				
			  </div>
				  
			  </div>
			  </form>
			  <%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
	<!--		  <%
	String id=request.getParameter("recipient-name");
	
	if (request.getParameter("id")!=null)
	{
		if (id!="")
		{
			if(allmethod.cekId(id) == "1"){
				
			}else{
				out.println("<script>alert('Gagal login!')</script>");
			}
		}else{
			out.println("<script>alert('Lengkapi data dahulu!')</script>");
		}
	}
%>-->
			</div>
		  </div>
		</div>
		   <!-- <a href="profile.jsp"><i class="fa fa-cog fa-2x" aria-hidden="true">  </i></a> -->
		<div class="w3-dropdown-hover">
		    <button class="w3-button w3-black">Settings</button>
		    <div class="w3-dropdown-content w3-bar-block w3-border">
		      <a href="profile.jsp" class="w3-bar-item w3-button">My Profile</a>
		      <a href="Login.jsp" class="w3-bar-item w3-button">Sign Out</a>
		    </div>
	  </div>
		  
	  </div>
	</nav>
	<!-- nav 2 -->
	<nav class="nav-fill navbar-fixed-top bg-light">
	  <div class="nav nav-tabs" id="nav-tab" role="tablist">
		<a class="nav-item nav-link active" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="true"><i class="fa fa-user fa-2x" aria-hidden="true"></i></a>
		<a class="nav-item nav-link" id="nav-chat-tab" data-toggle="tab" href="#nav-chat" role="tab" aria-controls="nav-chat" aria-selected="false"><i class="fa fa-commenting-o fa-2x" aria-hidden="true"></i></a>
		<a class="nav-item nav-link" id="nav-timeline-tab" data-toggle="tab" href="#nav-timeline" role="tab" aria-controls="nav-timeline" aria-selected="false"><i class="fa fa-clock-o fa-2x" aria-hidden="true"></i></a>
		<a class="nav-item nav-link" id="nav-more-tab" data-toggle="tab" href="#nav-more" role="tab" aria-controls="nav-more" aria-selected="false"><i class="fa fa-ellipsis-h fa-2x" aria-hidden="true"></i></a>
	  </div>
	</nav>
	<div class="tab-content" id="nav-tabContent">
	  <div class="tab-pane fade show active" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
	  
	  <%
		  if (request.getParameter("bContact")!=null)
		  {
			  String nama=allmethod.namauser(email.getValue());
			  String select = allmethod.startchat(email.getValue(), request.getParameter("fEmail"), nama, request.getParameter("fName"));
			  
			  response.sendRedirect("roomchat.jsp?id="+select);
		  }
	  %>
	  
	   <%
	   String carinama;
	   if (request.getParameter("btnSearch")!= null)
	   {
		   carinama=request.getParameter("search-friend");
	   
	   }else
	   {
		   carinama="";
	   }
	   
	    String select = allmethod.selectAllfriend(email.getValue(),carinama);
		String [] data = select.split(";");
		String [][] datajadi= new String[data.length][2];
		String nama="";
		if (data.length!=0)
		{
			for (int i=1;i<data.length;i++)
			{				
		       String[] temp= data[i].split("-");
			   datajadi[i][0]= temp[0];
			   datajadi[i][1]= temp[1];
			   
			}
			for (int j=1;j<datajadi.length;j++)
			{
				out.println("<form method='get' action='home.jsp'>");
				out.println("<input type='hidden' name='fName' value='"+datajadi[j][0]+"'/>");
				out.println("<input type='hidden' name='fEmail' value='"+datajadi[j][1]+"'/>");
				out.println("<button class='bodyContact' type='submit' name='bContact' style='background-color:transparent; border:none;'>"+
							"<i class='fa fa-user-circle-o fa-3x' aria-hidden='true'></i>  "+datajadi[j][0]+" </button> ");
				out.println("</form>");
			}
		}
		
		
	   %>
	  
	  </div>
	  <div class="tab-pane fade" id="nav-chat" role="tabpanel" aria-labelledby="nav-chat-tab" >
	  
	  <% 
	    String selectchat = allmethod.loadchatroom(email.getValue());
		String [] datachat = selectchat.split(";");
		String [][] datasplit= new String[datachat.length][3];
		if (data.length!=0)
		{
			for (int i=1;i<datachat.length;i++)
			{				
		       String[] temp= datachat[i].split("-");
			   datasplit[i][0]= temp[0];
			   datasplit[i][1]= temp[1];
			   datasplit[i][2] = temp[2];
			   //out.print(temp[0]+"-"+temp[1]);
			}
			
			for (int j=1;j<datachat.length;j++)
			{
				out.println("<a href='roomchat.jsp?id="+datasplit[j][2] +"'>"
						+"<div class='container' style='float:left;position:absolute;'>"
				  +"<img src='images.png' alt='Avatar' style='width:60px;height:60px;'>"
				  +"<p>"+datasplit[j][0]+"</p>"
				  +"<p>"+datasplit[j][1]+"</p>"
				  +"<span class='time-right'>11:00</span>"
				  +"</div>"
				+"</a><br><br><br><br>");
			}
		}
		
		
	   %>
	  <!-- 
	  <a href="roomchat.jsp">
			<div class="container" style="float:left;position:absolute;">
		  <img src="/w3images/bandmember.jpg" alt="Avatar" style="width:100%;">
		  <p>Hello. How are you today?</p>
		  <span class="time-right">11:00</span>
		</div>
		</a>
	   -->
		
	  </div>
	  <div class="tab-pane fade" id="nav-timeline" role="tabpanel" aria-labelledby="nav-timeline-tab">
	  	<%
	  		out.print(allmethod.selectTimeline(email.getValue()));
	  	%>
	  
	  
	  </div>
	  <div class="tab-pane fade" id="nav-more" role="tabpanel" aria-labelledby="nav-more-tab">
		<div class="bodyMore">
		 <button type="button" class="btn-primary-outline" style="background-color:transparent; border:none; width : 33%;" data-toggle="modal" data-target="#modalAddFriend">
		  <i class="fa fa-user-plus fa-5x" aria-hidden="true"></i>
		 </button>
	
	 
			<div class="modal fade" id="modalAddFriend" tabindex="-1" role="dialog" aria-labelledby="modalAddFriend" aria-hidden="true">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>
				  <form method="get" action="addfriend.jsp">
				  <div class="modal-body">
					
					  <div class="form-group">
						<input type="text" class="form-control" id="recipient-name" name="search-name" placeholder="search by id user">
						<!-- <%
				  if (request.getParameter("btnSearchid")!=null)
				  {
					    String select3 = allmethod.selectUser(request.getParameter("search-name"));
						String [] data3 = select3.split(";");
						if (data3.length!=0)
						{
							for (int i=1;i<data3.length;i++)
							{
								out.println("<button class='bodyContact' style='background-color:transparent; border:none;'>"+
											"<i class='fa fa-user-circle-o fa-3x' aria-hidden='true'></i>  "+data3[i]+" </button> ");
							}
						}
						
				  }	   				 
	   			 
				%>-->
					  </div>
					
				  </div>
				  <div class="modal-footer">
					<button type="submit" name="btnSearchid" class="btn btn-primary">Search</button>
				  </div>
				  </form>
				</div>
			  </div>
			</div>
			<form method="get" action="multichat.jsp">
			<button type="submit" class="btn-primary-outline" name="btnMulti"  style="background-color:transparent; border:none; width : 33%;"><i class="fa fa-users fa-5x" aria-hidden="true"></i></button>
			</form>
		 <button type="submit" class="btn-primary-outline" style="background-color:transparent; border:none; width : 33%;"><i class="fa fa-user fa-5x"></i></button>
	  </div>
	</div>
	
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
