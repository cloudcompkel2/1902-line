<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>LINE : Panggilan & Pesan Gratis</title>
</head>
<style type="text/css">
 *						{background-color: #F9F9F9;box-sizing: border-box;}
 .headerlogin 			{width: 100%; height:100%; align-content: center;margin-bottom: 50px;}
 .headerlogin .logo 	{display:block; margin-left:auto; margin-right:auto; margin-top: 90px;width: 150px; height: 55px}
 .bodyregis 			{width: 100%; height:100%; align-content: center; margin-top: 60px;}
 .bodyregis .centerbox 	{display:block;background-color:white; margin-left:auto;padding:5px;width:220px;height:33px; margin-right:auto; margin-top: 0px;}
 .bodyregis .warn		{display:block;background-color:#F9F9F9; margin-left:auto;padding:5px;width:220px;height:33px; margin-right:auto; margin-top: 10px; font-weight:bolder; color:#5B5B5B; text-align: justify;}
 .bodyregis .warn .link	{display:block;background-color:#F9F9F9; margin-left:auto;padding:5px;width:220px;height:33px; margin-right:auto; margin-top: 10px; font-weight:bolder; color:#5B5B5B; text-align: justify;}
 .bodyregis .btnregis   {display:block;background-color:white; margin-left:auto;padding:5px;width:220px;height:33px; margin-right:auto; margin-top: 80px; font-weight: bolder; color:#5B5B5B;}

</style>
</head>
<body>
<div class="headerlogin">
	<a href="http://localhost:8181/Login.jsp"><img src="logo.png" class="logo" style=""></a>
</div>
<div class="bodyregis">
	<form method="post">
		<input type="text" class="centerbox" placeholder="Alamat Email" name="tEmail"/>
		<input type="text" class="centerbox" placeholder="Nomor Telepon" name="tHp"/>
		<input type="password" class="centerbox" placeholder="Kata Sandi" name="tPass"/>
		<input type="password" class="centerbox" placeholder="Ketik Ulang Kata Sandi" name="tConfirm"/>
		<div class="warn">Dengan mengetuk tombol daftar, anda akan dianggap menyetujui <a href="http://terms.line.me/line_terms?lang=en">Syarat dan Ketentuan Penggunaan</a> serta <a href="http://terms.line.me/line_rules?lang=en">Kebijakan Privasi LINE</a></div>
		<button type="submit" class="btnregis" name="bregis" value="Register">Daftar</button>
	</form>
	<jsp:useBean id="allmethod" class="edu.stts.allmethod"></jsp:useBean>
</div>
</body>
<%
	String email=request.getParameter("tEmail");
	String hp=request.getParameter("tHp");
	String pass=request.getParameter("tPass");
	String confirm=request.getParameter("tConfirm");
	Boolean cekEmail = allmethod.isEmailValid(email);
	
	if (request.getParameter("bregis")!=null)
	{
		if (email!=null && hp !=null && cekEmail)
		{
			if (pass.equals(confirm))
			{
				out.println("<script>alert('"+allmethod.connectdb()+"')</script>");	
				out.println("<script>alert('"+allmethod.newuser(email,hp,pass)+"')</script>");
				response.sendRedirect("Login.jsp");
				//out.println("<script>alert('Pendaftaran Akun Gagal')</script>");
			}
			else
			{
				out.println("<script>alert('Pendaftaran Akun Gagal')</script>");	
				
			}
		}
		else if(!cekEmail){
			out.println("<script>alert('Email tidak valid!')</script>");
		}
		else{
			out.println("<script>alert('Lengkapi data dahulu!')</script>");
		}
	}
%>
</html>